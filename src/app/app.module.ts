import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { SideMenuComponent } from './side-menu.component';
import { ListBoxComponent } from './list-box.component';
import { ImgBoxComponent } from './img-box.component';
import {TopMenuComponent} from "./top-menu.component";


@NgModule({
  declarations: [
    AppComponent,
    SideMenuComponent,
    ListBoxComponent,
    ImgBoxComponent,
    TopMenuComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
