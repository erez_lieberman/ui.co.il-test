import { Component, Input } from '@angular/core';

@Component({
    selector: 'side-menu',
    template: `
	    <ul class="navbar-nav navbar-sidenav" [class.menu_closed]="isOpen">
		    <div class="logo_container">
			    <a class="navbar-brand" href="index.html">
				    <img src="../assets/images/logo.png" width="182"/>
			    </a>
		    </div>
		    <li *ngFor="let item of menuItems let i = index" [attr.data-index]="i" [ngClass]="[i==0 ? 'current' : '']" class="nav-item" data-toggle="tooltip" data-placement="right" title="{{ item.title }}">
			    <a class="nav-link">
                    <img src="{{ item.img }}">
				    <span class="nav-link-text">{{ item.title }}</span>
			    </a>
		    </li>
	    </ul>
	    `,
    styleUrls: ['./side-menu.component.scss']
})
export class SideMenuComponent {
    @Input() isOpen: boolean;
    menuItems = [
        {
            title: "Home",
            img: 'assets/images/sidenav_icons/home_icon.png'
        },
        {
            title: "Workflow",
            img: 'assets/images/sidenav_icons/Workflow_icon.png'
        },
        {
            title: "Statistics",
            img: 'assets/images/sidenav_icons/Statistics_icon.png'
        },
        {
            title: "Calendar",
            img: 'assets/images/sidenav_icons/Calendar_icon.png'
        },
        {
            title: "Users",
            img: 'assets/images/sidenav_icons/Users_icon.png'
        },
        {
            title: "Settings",
            img: 'assets/images/sidenav_icons/Settings_icon.png'
        },
    ];
}