import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav" (window:resize)="onResize($event)">
		        <div class="collapse navbar-collapse" id="navbarResponsive" [class.menu_closed]="isOpen">
                    <top-menu style="width: 100%" (menuClicked)="handleMenuClicked($event)"></top-menu>
			        <side-menu 
                        [isOpen] = this.isOpen
                    ></side-menu>
		        </div>
	        </nav>
	        <div class="content-wrapper" [class.menu_closed]="isOpen">
		        <div class="container-fluid">
                    <div class="row">
                        <div class="col">
                            <h1>Hello John!</h1>
                        </div>
                    </div>
                    <div class="row">
				        <div *ngFor="let ImagesBox of ImagesBoxes; let i = index" [attr.data-index]="i" [ngClass]="[i==0 ? 'col-xl-5' : 'col-xl-7']">
                            <img-box 
                                    [img]='ImagesBoxes[i].img'
                            ></img-box>
                        </div>
			        </div>
			        <div class="row">
				        <div class="col-xl-4" *ngFor="let listBox of listBoxes; let i = index" [attr.data-index]="i">
                            <list-box 
                                    [title]='listBoxes[i].title'
                                    [notification_blue]='listBoxes[i].notification_blue'
                                    [notification_pink]='listBoxes[i].notification_pink'
                                    [listItems]='listBoxes[i].listItems'
                                    [typeOfList]='listBoxes[i].typeOfList'
                            >
                            </list-box>
				        </div>
			        </div>
		        </div>
            </div>
      
    `,
  styleUrls: ['./app.component.scss']
})



export class AppComponent {

    isOpen = false;

    handleMenuClicked(value) {
        this.isOpen = value;
    }

    ngOnInit(){
        if(window.innerWidth < 991){
            this.isOpen = true;
        }
    }
    onResize(event) {
        if(event.target.innerWidth < 991){
            this.isOpen = true;
        }else{
            this.isOpen = false;
        }
    }

    listBoxes = [
        {
          title: 'Tasks',
          notification_blue: 5,
          notification_pink: 2,
          typeOfList: 'tasks',
          listItems: [
              {
                  img: "assets/images/n_icon.png",
                  title:"New website for Symu.co",
                  time:"5 days delays"
              },
              {
                  img: "assets/images/f_icon.png",
                  title:"Free business PSD Template ",
                  time:"2 days delays"
              },
              {
                  img: "assets/images/n_icon.png",
                  title:"New logo for JCD.pl",
                  time:"5 days left"
              },
              {
                  img: "assets/images/f_icon.png",
                  title:"Free Icons Set vol. 3",
                  time:"10 days left"
              },
          ]
        },
        {
          title: 'Messages',
          notification_blue: 2,
          typeOfList: 'messages',
          listItems: [
                {
                    img: "assets/images/photo-1441307811206-a12c74889338.png",
                    name: "Nina Jones",
                    title:"Hey You! It’s me again :-) I attached new",
                    time:"5 days delays"
                },
              {
                  img: "assets/images/photo-1443381301867-5a36ffaafc42-kopia.png",
                  name: "Nina Jones",
                  title:"I attached some new PSD files for",
                  time:"5 days delays"
              },
              {
                  img: "assets/images/photo-1441307811206-a12c74889338.png",
                  name: "Nina Jones",
                  title:"Good morning, you are fired",
                  time:"5 days delays"
              },
              {
                  img: "assets/images/photo-1443381301867-5a36ffaafc42-kopia.png",
                  name: "Nina Jones",
                  title:"Hello! Could You bring me coffee please",
                  time:"5 days delays"
              },
            ]
        },
        {
            title: 'Activity',
            notification_blue: 10,
            typeOfList: 'activity',
            listItems: [
                {
                    img: "assets/images/photo-1441307811206-a12c74889338.png",
                    name: "Nina Jones",
                    title:"added a new project",
                    projectName:"Free UI Kit",
                    time:"Just now"
                },
                {
                    img: "assets/images/photo-1443381301867-5a36ffaafc42-kopia.png",
                    name: "James Smith",
                    title:"commented project",
                    projectName:"Free PSD Template",
                    time:"40 minutes ago"
                },
                {
                    img: "assets/images/photo-1428931996691-a5108d4cdbf5.png",
                    name: "Alex Clooney",
                    title:"completed task",
                    projectName:"Symu Website",
                    time:"1 hour ago"
                },
                {
                    img: "assets/images/photo-1442458370899-ae20e367c5d8.png",
                    name: " Alexandra Spears",
                    title:"added a new project",
                    projectName:" Free PSD ",
                    time:"3 hours ago"
                },
            ]
        },
    ];


    ImagesBoxes = [
        {
            img: 'assets/images/your-sales.jpg'
        },
        {
            img: 'assets/images/active-users-kopia.png'
        },

    ];


}
