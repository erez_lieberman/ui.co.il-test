import { Component, Input } from '@angular/core';


@Component({
    selector: 'list-box',
    template: `
	    <div class="list-box-container">
            <div class="list-box-header">
                <div class="title">{{title}}</div>
                <div class="notification blue" *ngIf="notification_blue">{{notification_blue}}</div>
	            <div class="notification pink" *ngIf="notification_pink">{{notification_pink}}</div>
            </div>
            <div class="list-box-content">
	            <ul ngClass="{{typeOfList}}">
		            <li *ngFor="let listItem of listItems">
			            <!--<div class="img_container">-->
                            <img src="{{listItem.img}}" />
			            <!--</div>-->
                        <div [ngSwitch]="typeOfList">
	                        <div *ngSwitchCase="'tasks'" >
		                        <div class="details">
			                        {{listItem.name}}
			                        <div class="title">
				                        {{listItem.title}}
			                        </div>
			                        <div class="time">
				                        {{listItem.time}}
			                        </div>
		                        </div>
	                        </div>
	                        <div *ngSwitchCase="'messages'" >
                                <div class="details">
                                    <b>{{listItem.name}}</b> 
                                    <div class="time">{{listItem.time}}</div>
                                    <p>{{listItem.title}}</p>
                                    <div class="icons">
                                        <div class="share"></div>
	                                    <div class="settings"></div>
                                    </div>
                                </div>
                            </div>
	                        <div *ngSwitchCase="'activity'" >
		                        <div class="details">
                                    <div class="texts">
			                            <b>{{listItem.name}}</b> {{listItem.title}} <b>{{listItem.projectName}}</b>
                                    </div>
			                        <div class="time">{{listItem.time}}</div>
		                        </div>
	                        </div>
                        </div>
			            <div *ngIf="typeOfList == 'tasks'" class="dots">
				            <img src="../assets/images/options.png"/>
			            </div>
		            </li>
	            </ul>
            </div>
	    </div>
	    `,
    styleUrls: ['./list-box.component.scss']
})
export class ListBoxComponent {
    @Input() title: string;
    @Input() notification_blue: number;
    @Input() notification_pink: number;
    @Input() listItems: string[];
    @Input() typeOfList: string;

}