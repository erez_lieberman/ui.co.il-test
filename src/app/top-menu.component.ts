import { Component, Output, EventEmitter } from '@angular/core';

let isOpen = false;

@Component({
    selector: 'top-menu',
    template: `
	    <div class="top-menu-container" [class.menu_closed]="!this.isOpen">
		    <ul class="navbar-nav ml-auto">
			    <li class="nav-item">
				    <div class="menu_toggle_icon" (click)="changeMenu()"></div>
			    </li>
			    <li class="nav-item">
				    <div class="search_icon"></div>
			    </li>
			    <li class="nav-item add_project">
				    <button class="plus"> Add project</button>
			    </li>
			    <li class="nav-item">
				    <div class="messages_icon"></div>
			    </li>
			    <li class="nav-item">
				    <div class="notifications_icon">
                        <div class="numbers">
                            3
                        </div>
                    </div>
			    </li>
			    <li class="nav-item">
				    <div class="profile_box">
                        <img src="../assets/images/profile.png">
                        <div class="down_arrow"></div>
				    </div>
			    </li>
		    </ul>
	    </div>
	    `,
    styleUrls: ['./top-menu.component.scss']
})



export class TopMenuComponent {

    @Output() menuClicked = new EventEmitter();

    isOpen = !isOpen;

    changeMenu() {
        this.isOpen = isOpen;
        isOpen = !isOpen;
        this.menuClicked.emit(isOpen);
    }

}
