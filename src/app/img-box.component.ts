import { Component, Input } from '@angular/core';


@Component({
    selector: 'img-box',
    template: `
	    <div class="img-box-container">
            <img src="{{img}}" />
	    </div>
	    `,
    styleUrls: ['./img-box.component.scss']
})
export class ImgBoxComponent {
    @Input() img: string;
}
